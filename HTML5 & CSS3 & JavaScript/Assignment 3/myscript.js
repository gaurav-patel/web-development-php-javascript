    //************************************************************************************//
    //* Name :  Gauravkumar Patel                                                    
    //* Zenit login : int222_141b18                                                  
    //************************************************************************************//

function validationForPayment(){   

    //************************************************************************************//
    //*   You will need to call the functions that validate the following:               *//
    //************************************************************************************//
    //*        (1)              (2)              (3)             (4)                 
    //************************************************************************************//
    //*   Property value  -  Down payment  -  Interest rate -  Amortization              *//
    //************************************************************************************//
    //*   If there are no errors, then call                                              *//
    //*                                                                                  *//
    //*      detailPaymentCalculation(...., ......, ......, ......);                     *//
    //*                                                                                  *//
    //*   and make sure to pass the four values in the order shown above.                *//
    //*                                                                                  *//
    //************************************************************************************//
    //*   If there are errors, simply update the comments area with the message:         *//
    //*   Please complete the form first and then click on Calculate Monthly Payment     *//
    //*                                                                                  *//
    //************************************************************************************//
var errors = 0;

	errors += newCheckPr();
	errors += downpayment();
	errors += rate();
	errors += NumOfYears();


	if(errors == 0){
		var checkProperties = document.getElementById("propValue").value;
		var downpay = document.getElementById("downPay").value;
		var interest = document.getElementById("intRate").value;
		var amortization = document.getElementById("amortization").value;
		
		detailPaymentCalculation(checkProperties,downpay,interest,amortization);
	}

} // End of validationForPayment function

    //************************************************************************************//
    //*   Do not modify any statements in detailPaymentCalculation function          
    //************************************************************************************//

function detailPaymentCalculation(mortAmount,mortDownPayment,mortRate,mortAmortization) {

    //************************************************************************************//
    //*   This function calculates the monthly payment based on the following:           *//
    //*                                                                                  *//
    //*               M = P [ i(1 + i)n ] / [ (1 +  i)n - 1]                             *//
    //*                                                                                  *//
    //************************************************************************************//
     var paymentError = "";
     var v = mortAmount * 1;
     var d = mortDownPayment * 1;
     var i = mortRate * 1;
     var y = mortAmortization * 1;
     var a = v - d;
         i = i/100/12;
         n = y * 12;
     var f = Math.pow((1+i),n);

     var p = (a * ((i*f)/(f-1))).toFixed(2);

     if (p=="NaN" || p=="Infinity") {
         paymentError = "Please complete the form before attempting to calculate the monthly payment";
         document.forms[0].comments.value = paymentError;
         document.forms[0].payment.value = "";
     }
     else {
           document.forms[0].payment.value = p;
           document.forms[0].comments.value = "";
     }

} // End of detailPaymentCalculation function


function completeFormValidation() {

    //************************************************************************************//
    //*                                                                                  *//
    //* This function calls the different functions to validate all required fields      *//
    //*                                                                                  *//
    //* Once you have validated all field,                                               *//
    //* determine if any error(s) have been encountered                                  *//
    //*                                                                                  *//
    //* If any of the required fields are in error:                                      *//
    //*                                                                                  *//
    //*    present the client with a list of all the errors in reserved area             *//
    //*         on the form and                                                          *//
    //*          don't submit the form to the CGI program in order to allow the          *//
    //*          client to correct the fields in error                                   *//
    //*                                                                                  *//
    //*    Error messages should be meaningful and reflect the exact error condition.    *//
    //*                                                                                  *//
    //*    Make sure to return false                                                     *//
    //*                                                                                  *//
    //* Otherwise (if there are no errors)                                               *//
    //*                                                                                  *//
    //*    Change the 1st. character in the field called client to upper case            *//
    //*                                                                                  *//
    //*    Change the initial value in the field called jsActive from OFF to ON          *//
    //*                                                                                  *//
    //*    When a browser submits a form to a CGI program, disabled fields               *//
    //*    like the payment field are not included. To insure that the payment field     *//
    //*    is sent to the CGI, include the following JavaScript statement                *//
    //*    document.forms[0].payment.disabled = false;                                   *//
    //*                                                                                  *//
    //*    Make sure to return true in order for the form to be submitted to the CGI     *//
    //*                                                                                  *//
    //************************************************************************************//
var errors = 0;
document.getElementById("reserverd").innerHTML=("");
document.getElementById("reserverd").innerHTML+=("<b>Correct Following: </b>");
document.getElementById("reserverd").innerHTML+=("<ul>");	
	errors += userid();
	if(errors < 11)
		errors += client(); 
	if(errors < 11)
		errors += newCheckPr();
	if(errors < 11)
		errors += downpayment();
	if(errors < 11)
		errors += income();	
	if(errors < 11)
		errors += propdetails();
	if(errors < 11)
		errors += loc();
	if(errors < 11)
		errors += year();
	if(errors < 11)
		errors += month();
	if(errors < 11)
		errors += rate();
	if(errors < 11)	
		errors += NumOfYears();
document.getElementById("reserverd").innerHTML+=("</ul>");

	if(errors == 0){
		document.getElementById("jsActive").value = "ON";
		var name = document.getElementById("client").value;
		name = name.charAt(0).toUpperCase() + name.slice(1);
		document.getElementById("client").value = name;
		document.mortgage.payment.disabled = false;
		return true;
	}else{
		return false;
	}

} // End of completeFormValidation

//**************************validation for UserID**********************************************************//
function userid(){
	var numerrors = 0;
	var id = document.getElementById("userId").value;


	if(id.length != 10){
		document.getElementById("reserverd").innerHTML+=("<li>UserID must be length of 10.</li>");
		numerrors++;
	}

	if(id[4] != '-'){
		document.getElementById("reserverd").innerHTML+=("<li>UserID position 5 should be ' - ' </li>");
		numerrors++;
	}

	
	for(var i = 0; i < 4; i++){
		if(id[i] >= 0 && id[i] <= 9){
			//valid....
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>UserId should contain only numbers. </li>");
			numerrors++;
			i = 4;
		}
	}

	for(var i = 5; i < 10; i++){
		if(id[i] >= 0 && id[i] <= 9){
			//valid....
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>UserId should contain only numbers and one \" - \". </li>");
			numerrors++;
			i = 11;
		}
	}

	
	if(numerrors == 0){
		var sum = ((id[0] - '0')+(id[1] - '0')+(id[2] - '0')+(id[3] - '0')) * 2 + 1;
		var sum2 = (id[5] - '0')+(id[6] - '0')+(id[7] - '0')+(id[8] - '0')+(id[9] - '0');
		
		if(sum != sum2){
			document.getElementById("reserverd").innerHTML+=("<li> UserId is not valid. Try again! </li>");
			numerrors++;
		}
	}
	
return numerrors;
}

//************************validation for Client************************************************************//
function client(){

	var numerrors = 0;
	var name = document.getElementById("client").value;
	var count = 0;
	var index = 0;
	var len = name.length;
	if(len > '3'){
		//valid entry...
	}else{
		document.getElementById("reserverd").innerHTML+=("<li>Name must be more then 3 characters. </li>");
		numerrors++;
	}
	
	if((name[0] >= 'a' || name[0] >='A')&&(name[0] <= 'z' || name[0] <='Z')){
		//isvalid....
	}else{
		document.getElementById("reserverd").innerHTML+=("<li>Name must start with (A to Z) or (a to z). </li>");
		numerrors++;
	}

	for(var i = 0; i < name.length; i++){
		if((name[i] >= 'a' || name[i] >='A')&&(name[i] <= 'z' || name[i] <='Z')){
			//isvalid....
		}else if(name[i] == '\'' || name[i] == '-'){
			index = i;
			count++;
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Name has invalid character. only allowed are (a-z,-,'). </li>");
			i = name.length;
			numerrors++;
		}
	}	
	
	if((name[len-1] >= 'a' || name[len-1] >='A')&&(name[len-1] <= 'z' || name[len-1] <='Z')){
		//valid data..
	}else{
		document.getElementById("reserverd").innerHTML+=("<li>Name must end with (A to Z) or (a to z). </li>");
		numerrors++;
	}

	if(count == 2){
		if(name[index - 1] == "'" || name[index - 1] == "-"){
			document.getElementById("reserverd").innerHTML+=("<li>can not have (-) and (') next to each other. </li>");
			numerrors++;
		}else if(name[index + 1] == "'" || name[index + 1] == "-"){
			document.getElementById("reserverd").innerHTML+=("<li>can not have (-) and (') next to each other. </li>");
			numerrors++;
		}
	}else if(count > 2){
		document.getElementById("reserverd").innerHTML+=("<li>can not include more then one of these (-) or (') . </li>");
		numerrors++;
	}

return numerrors;
}

//************************validation for PropValue************************************************************//
function newCheckPr() {
	var errors = 0;
	
	var value = document.getElementById("propValue").value;
	var downpay = Number(document.getElementById("downPay").value) + 65000;
	
	if(value % 1 == 0){
		if(value > downpay){
		//valid property value..		
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Property value must be grater then $65,000 + down payment. </li>");
			errors++;
		}
	}else{
		document.getElementById("reserverd").innerHTML+=("<li>Property value must be postive, no decimal and only numbers. </li>");
		errors++;
	}

return errors;
}

//************************validation for Down Payment************************************************************//
function downpayment(){

	var errors = 0;
	var value = document.getElementById("downPay").value;
	var min = Number(document.getElementById("propValue").value) * 0.10;

	if(value % 1 == 0){
		if(value > min){
		//valid down payment...
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Minimum Down Payment required. (10% of property value) </li>");
			errors++;
		}
	}else{
		document.getElementById("reserverd").innerHTML+=("<li>Property value must be postive, no decimal and only numbers. </li>");
		errors++;
	}

return errors;	
}

//************************validation for Income**********************************************************//
function income(){
	var error = 0;
	var value = document.getElementById("income");
	var check = false;
	
	for(var i = 0; i < 10; i++){
		if(value.options[i].selected){
			check = true;
			i = 11;
		}else{
			check = false;
		}
	}
	if(check == false){
		document.getElementById("reserverd").innerHTML+=("<li>Please select income range. </li>");
		error++;
	}
	
return error;
}

//************************validation for Property Details*******************************************************//
function propdetails(){
	var error = 0;
	var detail = document.getElementsByName("propDetails");
	var check = false;

	for(var i = 0; i < 7; i++){
		if(detail[i].checked == true){
			check = true;
			i = 7;
		}
	}
	if(check == false){
		document.getElementById("reserverd").innerHTML+=("<li>Need to select from Property Details. </li>");
		error++;
	}

return error;
}

//**********Onclick event for selection on all of the above in property Details***********************************//
function selectall(){
	var any = document.getElementById("propDetails7").checked;
	
	if(any == true){
		document.getElementById("propDetails1").checked = false;
		document.getElementById("propDetails2").checked = false;
		document.getElementById("propDetails3").checked = false;
		document.getElementById("propDetails4").checked = false;
		document.getElementById("propDetails5").checked = false;
		document.getElementById("propDetails6").checked = false;
	}
}

//************************validation for Property Location*******************************************************//
function loc(){
	var error = 0;
	var selection = document.getElementsByName("propLocation");
	check = false;
	
	for(var i = 0; i < 5; i++){
		if(selection[i].checked == true){
			check = true;
			i = 5;
		}
	}
	if(check == false){
		document.getElementById("reserverd").innerHTML+=("<li>Please select from Property Location. </li>");
		error++;
	}
return error;
}

//************************validation for Year*****************************************************//
function year(){
	var error = 0;
	var usr = document.getElementById("mortYear").value;
	var comp_date = new Date();
	var year = comp_date.getFullYear();
	
	if((usr[0] >= "a" || usr[0] >= "A") && (usr[0] <= "z" || usr[0] <= "Z")){
		document.getElementById("reserverd").innerHTML+=("<li>Please input Year in Numbers(####).  </li>");
		error++;
	}else{
		if(usr[0] != " "){
			if(usr % 1 != 0){
				document.getElementById("reserverd").innerHTML+=("<li>No decimal places allowed in year.  </li>");
				error++;
			}else if((usr == year) == false && (usr == (year + 1)) == false){
				document.getElementById("reserverd").innerHTML+=("<li>Must be current year or next year.  </li>");
				error++;
			}
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Please enter year(####).  </li>");
			error++;
		}
	}

return error;
}

//************************validation for Month*******************************************************//
function month(){
	var error = 0;
	var usr =  document.getElementById("mortMonth").value;
	var comp_date = new Date();
	var month = comp_date.getMonth();
	if((usr[0] >= "a" || usr[0] >= "A") && (usr[0] <= "z" || usr[0] <= "Z")){
		document.getElementById("reserverd").innerHTML+=("<li>Please input Month in Numbers(##).  </li>");
		error++;
	}else{
		if(usr[0] != " "){
			if(usr > 0 && usr < 13){
				if(usr % 1 != 0){
					document.getElementById("reserverd").innerHTML+=("<li>No decimal places allowed in Month.  </li>");
					error++;
				}else if((usr == month + 1) == false && (usr == (month + 2)) == false){
					document.getElementById("reserverd").innerHTML+=("<li>Must be current Month or next Month.  </li>");
					error++;
				}
			}else{
				document.getElementById("reserverd").innerHTML+=("<li>Month should be in between 1 to 12.  </li>");
				error++;
			}
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Please enter Month(##).  </li>");
			error++;
		}
	}
return error;
}

//************************validation for Interest Rate*****************************************************//
function rate(){
	var error = 0;
	var usr = document.getElementById("intRate").value;
	
	if(usr[0] == " "){
		document.getElementById("reserverd").innerHTML+=("<li>Must include interest rate. </li>");
		error++;
	}else{
		if(usr >= 2.000 && usr <= 7.000){
			//valid interest rate...
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>Interest rate must be in between 2.000 to 7.000  </li>");
			error++;
		}
	}
return error;
}

//************************validation for Amortization*******************************************************//
function NumOfYears(){
	var error = 0;
	var usr = document.getElementById("amortization").value;

	if(usr[0] == " "){
		document.getElementById("reserverd").innerHTML+=("<li>Must have value in No of Years.  </li>");
		error++;
	}else{
		if(usr >= 5 && usr <= 20){
			//valid input...
		}else{
			document.getElementById("reserverd").innerHTML+=("<li>No of Years should be in between 5 to 20.  </li>");
			error++;
		}
	}
	
return error;
}