//***********************************************************************************//
//* Name : Gauravkumar Patel                                                                         *//
//* Zenit login : int222_141b18                                                       *//
//***********************************************************************************//


var myMonths=new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
var myDays= new Array("Sun","Mon","Tue","Wed","Thu","Fri","Sat","Sun");
var today=new Date();
var thisDay=myDays[today.getDay()]
var thisMonth=myMonths[today.getMonth()]
var thisYear=today.getFullYear()
var thisDate=today.getDate()
var todaysDate=thisDay+", "+thisDate+" "+thisMonth+" "+thisYear


function completeFormValidation() {
	var value = (document.getElementById("userId").value).length;
	var chk = true;
//************** Validation for user ID ***************//
	for(var i = 0; i < value; i++){
		if(document.getElementById("userId").value[i] >= "0" && document.getElementById("userId").value[i] <= "9"){
			chk = true;
		}else{
			chk = false;
			alert("Incorrect UserID");
			i = value;
		}
	}
//************** Validation for NAME ******************//
	if(chk == true){
		value = (document.getElementById("client").value).length;
		for(var i = 0; i < value; i++){
			if(document.getElementById("client").value[i] >= "a" && document.getElementById("client").value[i] <= "z"){
				check = true;
			}else{
				check = false;
				i = value;
				alert("Incorrect Name");
			}
		}
	}
//************** Validation for Property Value ***************//
	if(chk == true){
		value = (document.getElementById("propValue").value).length;
		for(var i = 0; i < value; i++){
			if(document.getElementById("propValue").value[i] >= "0" && document.getElementById("propValue").value[i] <= "9"){
				chk = true;
			}else{
				chk = false;
				alert("Incorrect Property Value");
				i = value;
			}
		}
	}
//************** Validation for Down Payment ***************//
	if(chk == true){
		if(document.getElementById("downPay").value < "0"){
			chk = false;
			alert("Incorrect Down Payment");
		}
	}
//************** Validation for Income Range ***************//
	if(chk == true){
		if(document.getElementById("income").value == false){
			alert("Please select Income Range");
			chk = false;
		}
	}
//************** Validation for Income Range ***************// Not working................................
/*	if(chk == true){
//alert("Value of PropDetails is : " + document.getElementById("propDetails").checked );
		if(document.getElementById("propDetails").checked == false){
			chk = false;
			alert("Please select Property Details");
		}
	}
*/
//************** Validation for Property Location ***************// Not working................................
/*	if(chk == true){
		var m = document.getElementById("propLocation");
		var cker = true;
		var more = false;
		for(var i = 0; i <= 5; i++){
			if(m[i].checked == true){ 
				alert("added one");
				more= true;
			}else{
				cker = false;
			}
		}
		if(cker == false ){
			chk = false;
			alert("Please select Property Location");
		}
	}
*/
//************** Validation for Year ***************//
	if(chk == true){
		value = (document.getElementById("mortYear").value).length;
		if(value < "4"){
			alert("Invalid year");
			chk = false;
		}
		if(document.getElementById("mortYear").value >= 0000 && document.getElementById("mortYear").value <= "9999"){
			//it's valid...
		}else{
			alert("Invalid Year Entered...");
			i = 5;
			chk = false;
		}
	}
//************** Validation for Month ***************//
	if(chk == true){
		if(document.getElementById("mortMonth").value > 0 && document.getElementById("mortMonth").value < 13){
		//it's valid...
		}else{
			alert("Invalid Month");
			chk = false;
		}
	}
//************** Validation for Interest Rate ***************//
	if(chk == true){
		var split = (document.getElementById("intRate").value).split('.');
//alert("1st split" + split[0]);
//alert("2nd split" + split[1]);

		for(var i = 0; i < split[0].length; i++){
			if(split[0][i] >= 0 && split[0][i] <= 9){
			//it's valid
			}else{
				i = 7;
				alert("Invalid Interest Rate");
				chk = false;
			}
		}
		
		for(var i = 0; i < split[0].length; i++){
			if(split[1][i] >= 0 && split[1][i] <= 9){
			//it's valid
			}else{
				i = 7;
				alert("Invalid Interest Rate");
				chk = false;
			}
		}
	}
//************** Validation for Years ***************//
	if(chk == true){
		if(document.getElementById("amortization").value[0] >= 1 && document.getElementById("amortization").value[0] <= 9){
			if(document.getElementById("amortization").value[1] >= 1 && document.getElementById("amortization").value[1] <= 9){
			//it's valid
			}else{
				alert("Incorrect Number of Years");
				chk = false;
			}
		}else{
			alert("Incorrect Number of Years");
			chk = false;
		}
	}
return chk;
} // End of completeFormValidation
