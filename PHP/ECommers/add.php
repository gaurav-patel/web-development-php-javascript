<?php
/*
Subject Code   : INT322
Student Name   : Gaurav Patel
Date Submitted : 14-Oct-2014

Student Declaration:

I/we declare that the attached assignment is my/our own work in accordance with Seneca Academic Policy. No part
of this assignment has been copied manually or electronically from any other source (including web sites) or
distributed to other students.

Name:      ____Gauravkumar Patel____
*/
	session_start();
	if(!isset($_SESSION['user'])){
		header("Location: login.php");
	}

	if(!isset($_SERVER['HTTPS'])){
		header("Location: https://zenit.senecac.on.ca/~int322_143d07/assign2/add.php");
	}
//Global variables for containing error messages.
$ok=true;
$name="";
$desc="";
$suppliercode="";
$cost="";
$price="";
$on_hand="";
$re_order_point="";

//Connecting to MySQL server
$secure = file('/home/int322_143d07/secret/topsecret');
$link = mysqli_connect(trim($secure[0]),trim($secure[1]),trim($secure[2]),trim($secure[3])) or die (mysqli_connect_error());	

	if(isset($_GET['id']) && !isset($_POST['id'])){
		$ok = false;
		$id = trim($_GET['id']);
		$query = "select * from inventory where id='$id'";
		$result = mysqli_query($link, $query) or die('query failed'. mysqli_error($link));
		$row = mysqli_fetch_assoc($result);
		$_POS['id'] = $id;
		$_POST['name'] = trim($row['itemName']);
		$_POST['description'] = trim($row['description']);
		$_POST['suppliercode'] = trim($row['supplierCode']);
		$_POST['cost'] = trim($row['cost']);
		$_POST['price'] = trim($row['price']);
		$_POST['onhand'] = trim($row['onHand']);
		$_POST['reorderpoint'] = trim($row['reorderPoint']);
		$_POST['backorder'] = trim($row['backOrder']);
	}

	//Validating form information.
	if($_POST){ 
		if($_POST['name'] != ""){
			//Validation rules for Name field: letters, spaces, colon, semi-colon, dash, comma, apostrophe and numeric character (0-9) only - cannot be blank
			if(!preg_match("/^[0-9a-z :;,'-]+$/i",trim($_POST['name']))){
				$name = "Only following are allowed: 'a-z' | '0-9' | ':' | ';' | '-' | ',' | '";
				$ok = false;
			}
		}else{
			$name = "Error: enter name.";
			$ok = false;
		}
		if($_POST['description'] != ""){
			//Validation rules for Description field: letters, digits, periods, commas, apostrophes, dashes and spaces only - cannot be blank
			if(!preg_match("/^[0-9a-z.,'\s-]+$/i",trim($_POST['description']))){
				$desc = "Only letters, numbers, colon, semi-colon, comma and apostrophe are allowed.";
				$ok = false;
			}
		}else{
			$desc = "Error: enter description.";
			$ok = false;
		}
		if($_POST['suppliercode'] != ""){
			//Validation rules for Supplier Code field: letters, spaces, numeric characters (0-9) and dashes only - cannot be blank
			if(!preg_match("/^[0-9a-z -]+$/i",trim($_POST['suppliercode']))){
				$suppliercode = "Only letters, numbers, spaces and dashes are allowed.";
				$ok = false;
			}
		}else{
			$suppliercode = "Error: enter supplier code.";
			$ok = false;
		}		
		if($_POST['cost'] != ""){
			//Validation rules for Supplier Code field: monetary amounts only i.e. one or more digits, then a period, then two digits - cannot be blank 
			if(!preg_match("/^[0-9]+.([0-9]){1,2}$/i",trim($_POST['cost']))){
				$cost = "Only numbers and two digits after decimal is Must.";
				$ok = false;
			}
		}else{
			$cost = "Error: enter cost.";
			$ok = false;
		}
		if($_POST['price'] != ""){
			//Validation rules for Supplier Code field: monetary amounts only i.e. one or more digits, then a period, then two digits - cannot be blank
			if(!preg_match("/^[0-9]+.([0-9]){1,2}$/i",trim($_POST['price']))){
				$price = "only numbers and digits after decimal place is Must.";
				$ok = false;
			}
		}else{
			$price = "Error: enter price.";
			$ok = false;
		}
		if($_POST['onhand'] != ""){
 			//Validation rules for Supplier Code field: digits only- cannot be blank
			if(!preg_match("/^[0-9]+$/i",trim($_POST['onhand']))){
				$on_hand = "Only digits are allowed.";
				$ok = false;
			}
		}else{
			$on_hand = "Error: enter on hand items.";
			$ok = false;
		}
		if($_POST['reorderpoint'] != ""){
			//Validation rules for Supplier Code field: digits only- cannot be blank
			if(!preg_match("/^[0-9]+$/i",trim($_POST['reorderpoint']))){
				$re_order_point = "Only digits are allowed.";
				$ok = false;
			}
		}else{
			$re_order_point = "Error: enter recorder point.";
			$ok = false;
		}
		if(isset($_POST['backorder']) != "checked"){ //Setting value for "backorder"
			$_POST['backorder'] = "n";
		}
	}
//Global variables to input data into table.	
$ID = trim($_GET['id']);
$Name = trim($_POST['name']);
$Desc = $_POST['description'];
$Code = trim($_POST['suppliercode']);
$Cost = trim($_POST['cost']);
$Price = trim($_POST['price']);
$Hand = trim($_POST['onhand']);
$Point = trim($_POST['reorderpoint']);
$Order = trim($_POST['backorder']);

	if($_POST && $ok){ //Sending form values to MySql server.
		$query = "";
		if(isset($_GET['id'])){
			$query = "update table inventory set itemName='". $Name . " ', description=' " . $Desc . " ', supplierCode=' " . $Code . "', cost=' " . $Cost . "', price=' " . $Price . "', onHand='  " . $Hand . " ', reorderPoint=' " . $Point . "' , backOrder=' " . $Order . "' where id=' " . $ID  . "'";
		}else{
		//Making query and submitting it
			$query = "Insert into inventory values itemName='$Name', description='$desc', supplierCode='$Code', cost='$Cost', price='$Price', onHand='$Hand', reorderPoint='$Point', backOrder='$Order'";
		}
		print_r($query);
		$result = mysqli_query($link, $query)or die('query failed '. mysqli_error($link));
		if($result){
			header("Location: view.php");
		}else{
			$error = 'Error submitting data, to try again click <a href="add.php">here</a>';
			echo $error;
		}
		mysqli_close($link); //Closing connection to MySql server.
	}else{ //If post is not set then display below html.
		
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Welcome to Cricket Star store</title>
		<link rel="stylesheet" href="a1.lib/a1_css.css" />
	</head>
	<body>
		<h1>Cricket Star - Cricket Equipments</h1>
		<ul class="menu">
			<li><a href="add.php">Add</a></li>
			<li><a class="link" href="view.php">View All</a></li>
			<li><form action="" method="post" id="search">
				Search in description: <input type="text" name="search" value="<?php if(isset($_SESSION['search'])) echo $_SESSION['search']; ?>" />
				<input type="submit" value="Search" />
			</form></li>
			<li>user: <?php echo $_SESSION['user']?></li>
			<li><a href="logout.php">Logout</a></li>
		</ul>

		<form method="post" action="">
			<table>
				<?php
					if(isset($_GET['id'])){
						$id = trim($_GET['id']);
					?>	<tr>
							<td>ID:</td>
							<td><input name="id" value="<?php if(isset($_GET['id'])) echo $_GET['id'];?>" readonly="readonly"/></td>
						</tr>
					<?php
					}
				?>
				<tr>
					<td>Name:</td>
					<td><input type="text" name="name" value="<?php if(isset($_POST['name'])) echo $_POST['name'];?>"></input></td><td class="err"><?php echo $name; ?></td>	
				</tr>
				<tr>
					<td>Description:</td>
					<td><textarea rows="2" cols="15" name="description"><?php if(isset($_POST['description'])) echo $_POST['description'];?></textarea></td><td class="err"><?php echo $desc; ?></td>
				</tr>
				<tr>
					<td>Supplier Code:</td>
					<td><input type="text" name="suppliercode" value="<?php if(isset($_POST['suppliercode'])) echo $_POST['suppliercode'];?>"></input></td><td class="err"><?php echo $suppliercode; ?></td>	
				</tr>
				<tr>
					<td>Cost:</td>
					<td><input type="text" name="cost" value="<?php if(isset($_POST['cost'])) echo $_POST['cost'];?>"></input></td><td class="err"><?php echo $cost;?></td>	
				</tr>
				<tr>
					<td>Price:</td>
					<td><input type="text" name="price" value="<?php if(isset($_POST['price'])) echo $_POST['price'];?>"></input></td><td class="err"><?php echo $price;?></td>	
				</tr>
				<tr>
					<td>On Hand:</td>
					<td><input type="text" name="onhand" value="<?php if(isset($_POST['onhand'])) echo $_POST['onhand'];?>"></input></td><td class="err"><?php echo $on_hand;?></td>	
				</tr>
				<tr>
					<td>Reorder Point:</td>
					<td><input type="text" name="reorderpoint" value="<?php if(isset($_POST['reorderpoint'])) echo $_POST['reorderpoint'];?>"></input></td><td class="err"><?php echo $re_order_point;?></td>	
				</tr>
				<tr>
					<td>Back Order:</td>
					<td><input type="checkbox" name="backorder" value="y"<?php echo (isset($_POST['backorder'])) && ($_POST['backorder'])?"checked":"";?>></input></td>	
				</tr>
				<tr>
					<td><input type="submit" value="Submit"/></td>
				</tr>
			</table>	
		</form>
		
		<footer>
			<p>ęCopyright by CricketStar Co.</p>
		</footer>
	</body>
</html>

<?php
	} //End of if($_POST && $ok)'s else
?>
