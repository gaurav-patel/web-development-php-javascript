<?php
session_start();

$secure = file('/home/int322_143d07/secret/topsecret');
$link = mysqli_connect(trim($secure[0]),trim($secure[1]),trim($secure[2]),trim($secure[3])) or die (mysqli_connect_error());
$ok = false;
$wel_msg = "";
$err = "";
    
    //Checking if user is logged in or not, if yes redirecting to view.php page.
    if(isset($_SESSION['user'])){ 
        header("Location: view.php");    
    }
    //Checking if the request is made through HTTPS, if not redirecting to logout.php to get secure connection.
    if(!isset($_SERVER['HTTPS'])){
        header("Location: https://zenit.senecac.on.ca/~int322_143d07/assign2/logout.php");
    }
    
    if(isset($_GET['forgot'])){
        if(isset($_POST['username'])){
            $user = $_POST['username'];
            $secure_username = htmlentities($user);
            $secure_username = addslashes($user);
            $query = "select * from users where username='$secure_username'";
            $result = mysqli_query($link,$query) or die('query failed' . mysqli_error($link));
            
            if($result){
                $row = mysqli_fetch_assoc($result);
                $passwd_hint = $row['passwordHint'];
                $to = trim($_POST['username']);
                $subject = "PasswordHint";
                $message = "Use this to retrive access for the site\n username : $to \n Password hint : $passwd_hint\n";
                mail($to, $subject, $message);
                header("Location: login.php");
            }else{
                header("Location: login.php");
            }
        }else{
    ?>
        <html>
            <head>
                <title>Password Reset</title>
                <link rel="stylesheet" href="a1.lib/a1_css.css" />
            </head>
            <body>
                <h1>Forgot Password</h1>
                <form method="post" action="">
                    <input type="text" name="username" /> &nbsp;
                    <input type="submit" value="Submit" />  <br />
                </form>
            </body>
        </html>
<?php
        }
        
    }else{
        $role = "";
        $secure_username = "";
        $secure_password = "";
        
        if($_POST){
            $user = $_POST['user_name'];
            $passwd = $_POST['password'];
            
            $secure_username = htmlentities($user);
            $secure_username = addslashes($secure_username);
            $secure_password = htmlentities($passwd);
            $secure_password = addslashes($secure_password);
            $crypt = crypt($secure_password, $secure_username);
            
            $query = "select * from users where username='$secure_username' and password='$crypt'";
            $result = mysqli_query($link,$query)or die('query failed' . mysqli_error($link));
            $row = mysqli_fetch_assoc($result);
                if($result && $row){
                        $role = $row['role'];
                        $ok = true;
                    }else{
                        $err = "Username/password is incorrect. Please try again.";
                    }
        }
    
        if($_POST && $ok){
            $_SESSION['user'] = $secure_username;
            $_SESSION['role'] = $role;
            header("Location: view.php");
        }
        else{
?>
        <html>
            <head>
                <title>Working with Sessions</title>
                <link rel="stylesheet" href="a1.lib/a1_css.css" />
            </head>
            <body>
                <h1>Login:</h1>
                <form method="post" action="" >
                    username : <input type="text" name="user_name" />      <br />
                    password : <input type="password" name="password" />   <br />
                    <input type="submit" value="Submit" /> <?php echo $err; ?> <br /> 
                    <a href="login.php?forgot=yes">Forgot Password</a>
                </form>
                
            </body>
        </html>
<?php
        }
    }
?>