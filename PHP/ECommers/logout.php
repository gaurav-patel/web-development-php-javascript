<?php 
    session_start();
    //checking if it's secure server, if it's not redirecting to secure server.
    if(!isset($_SERVER['HTTPS'])){ 
        header("Location: https://zenit.senecac.on.ca/~int322_143d07/assign2/logout.php");
    }
    
    //checking if sessing is already set if it is delete the session and redirect to login page.
    if(isset($_SESSION['user'])){ 
        session_unset();
        session_destroy();
        header("Location: login.php");
    }else{
        header("Location: login.php");
    }
?>