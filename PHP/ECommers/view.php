<?php
/*
Subject Code   : INT322
Student Name   : Gaurav Patel
Date Submitted : 14-Oct-2014

Student Declaration:

I/we declare that the attached assignment is my/our own work in accordance with Seneca Academic Policy. No part
of this assignment has been copied manually or electronically from any other source (including web sites) or
distributed to other students.

Name:      ____Gauravkumar Patel____
*/
    session_start();
    //Checking if user has logged in or not, if not redirecting to login page.
    if(!isset($_SESSION['user'])){
	header("Location: login.php");
    }
    
    //Checking if the server is https, if not redirecting to https server.
    if(!isset($_SERVER['HTTPS'])){
	header("Location: https://zenit.senecac.on.ca/~int322_143d07/assign2/view.php");
    }
    
    //Connection to MySql server
    $secure = file('/home/int322_143d07/secret/topsecret');
    $link = mysqli_connect(trim($secure[0]),trim($secure[1]),trim($secure[2]),trim($secure[3])) or die (mysqli_connect_error());
    
    //Declaring Globle variable for later use multiple times.
    $query = "";
    $err = "";
    $secure_search = "";
    $sort = "";
    //Sorting information from inventory table.
    if(isset($_GET['sort'])){
	$sort = trim($_GET['sort']);
	setcookie('sort', $sort, time()+(86400*30));
	header("Location: view.php");
    }
    //Assigning Cookie sort value to Globle variable for later use.
    if(isset($_COOKIE['sort'])){
	$sort = $_COOKIE['sort'];
    }else{
	$sort = "id";
    }
    
    //Special search case, checks for SQL Injection and XSS.
    if(isset($_POST['search'])){
	$search = trim($_POST['search']);
	$secure_search = htmlentities($search);
        $secure_search = addslashes($secure_search);
	
	$_SESSION['search'] = $secure_search;
	$query = "select * from inventory where description like '%$secure_search%' order by $sort";
    }else if(isset($_SESSION['search'])){
	$secure_search = $_SESSION['search'];
	$query = "select * from inventory where description like '%$secure_search%' order by $sort";
    }else{
	//Retreaving data from MySql server.
	$query = "select * from inventory order by $sort";
    }
    
    $result = mysqli_query($link, $query) or die(mysqli_error($link));
    
    //Checking if there is any rows to display from special search query result, if not error message is created.
    if(mysqli_num_rows($result) <= 0 && $_POST['search']){
	$err = "<p class='err'>No records found with this $search.</p>";
    }else if(mysqli_num_rows($result) == 0){
	$err = "<p class='err'>No items to display.</p>";
    }
?>
<html>
    <head>
        <title>View all records</title>
        <link rel="stylesheet" href="a1.lib/a1_css.css" />
    </head>
    
    <body>
        <h1>Cricket Star - Cricket Equipments</h1>
	<ul class="menu">
	    <li><a href="add.php">Add</a></li>
            <li><a class="link" href="view.php">View All</a></li>
	    <li><form action="" method="post" id="search">
		    Search in description: <input type="text" name="search" value="<?php if(isset($_SESSION['search'])) echo $_SESSION['search']; ?>" />
		    <input type="submit" value="Search" />
		</form></li>
	    <li>user: <?php echo $_SESSION['user']?></li>
	    <li><a href="logout.php">Logout</a></li>
	</ul>
    <?php
	    //Displaying error message if Special Search has return zero rows of data.
	    if($err != ""){
		echo $err;
	    }else{
    ?>
		<table class="view">
		     
		    <tr>
			<form action="" method="get">   
			    <th><a href="view.php?sort=id">ID</a></th>
			    <th><a href="view.php?sort=itemName">Item Name</a></th>
			    <th><a href="view.php?sort=description">Description</a></th>
			    <th><a href="view.php?sort=supplierCode">Supplier Code</a></th>
			    <th><a href="view.php?sort=cost">Cost</a></th>
			    <th><a href="view.php?sort=price">Price</a></th>
			    <th><a href="view.php?sort=onHand">Number On Hand</a></th>
			    <th><a href="view.php?sort=reorderPoint">Reorder Level</a></th>
			    <th><a href="view.php?sort=backOrder">On Back Order?</a></th>
			    <th><a href="view.php?sort=deleted">Delete Flag</a></th>
			    <th><a href="view.php?sort=deleted">Delete/Restore</a></th>
			</form>
		    </tr>
		    
                <?php
		    //Displaying information from inventory table.
		    while($row = mysqli_fetch_assoc($result)){
                ?>
			<form action="" method="get">
			    <tr>
				<td><a href="add.php?id=<?php echo $row['id'] ?>"><?php print $row['id']; ?></a></td>
				<td><?php print $row['itemName']; ?></td>
				<td><?php print $row['description']; ?></td>
				<td><?php print $row['supplierCode']; ?></td>
				<td><?php print $row['cost']; ?></td>
				<td><?php print $row['price']; ?></td>
				<td><?php print $row['onHand']; ?></td>
				<td><?php print $row['reorderPoint']; ?></td>
				<td><?php print $row['backOrder']; ?></td>
				<td><?php print $row['deleted']; ?></td>
				<td><a href="delete.php?id=<?php print $row['id']; ?>"><?php print ($row['deleted'] == "y")?"Restore":"Delete"; ?></a></td>
			    </tr>
			</form>
                <?php
                    }
		//End of displaying information from inventory table and closing connection to MySql server.
		mysqli_close($link);
                ?>
		</table>
	<?php
	    }
	?>
	<footer>
	    <p>ęCopyright by CricketStar Co.</p>
	</footer>
        
    </body>
</html>