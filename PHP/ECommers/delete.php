<?php
/*
Subject Code   : INT322
Student Name   : Gaurav Patel
Date Submitted : 14-Oct-2014

Student Declaration:

I/we declare that the attached assignment is my/our own work in accordance with Seneca Academic Policy. No part
of this assignment has been copied manually or electronically from any other source (including web sites) or
distributed to other students.

Name:      ____Gauravkumar Patel____
*/
    session_start();
    if(!isset($_SERVER['HTTPS'])){
        header("Location: https://zenit.senecac.on.ca/~int322_143d07/assign2/delete.php");
    }
    
    if(!isset($_SESSION['user'])){
        header("Location: login.php");
    }
    
    //Connecting to MySql server
    $secure = file('/home/int322_143d07/secret/topsecret');
    $link = mysqli_connect(trim($secure[0]),trim($secure[1]),trim($secure[2]),trim($secure[3])) or die (mysqli_connect_error());
    
    //Getting the data from MySql
    $id = $_GET['id'];
    $query = "select * from inventory where id=$id";
    $result = mysqli_query($link, $query) or die(mysqli_error($link));
    $result = mysqli_fetch_assoc($result);
    
    //Creating query to update data
    if($result['deleted'] == "y"){
        $query = "update inventory set deleted='n' where id=".$_GET['id'];
    }else{
         $query = "update inventory set deleted='y' where id=".$_GET['id'];
    }
    
    //Updating MySql database and disconnecting connection to MySql server.
    $result =  mysqli_query($link, $query) or die(mysqli_error($link));
    mysqli_close($link);
    
    //Returning back to view.php if there is no error.
    if($result){
        header("Location: view.php");
    }else{
        echo $result;
    }
?>
